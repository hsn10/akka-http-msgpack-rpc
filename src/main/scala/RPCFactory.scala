package com.filez.scala.akka.msgpack.rpc

import akka.stream.scaladsl.Flow
import akka.http.scaladsl.model.ws._

trait RPCFactory[T] {

   def messageHandler(obj: T) : Flow[Message, Message, Any]

}