name := "MsgPack RPC/Websocket for Akka-HTTP"

normalizedName := "akka-http-msgpack-rpc"

version := "0.0.1"

organization := "com.filez.scala.akka"

organizationName := "Filez.com"

startYear := Some(2020)

homepage := Some(url("https://gitlab.com/hsn10/akka-http-msgpack-rpc"))

licenses += "MIT" -> url("https://opensource.org/licenses/MIT")

developers += Developer("hsn10","Radim Kolar","hsn@sendmail.cz",url("https://netmag.ml"))

scmInfo := Some(ScmInfo(url("https://gitlab.com/hsn10/akka-http-msgpack-rpc"),"scm:git:https://gitlab.com/hsn10/akka-http-msgpack-rpc.git"))

crossScalaVersions := Seq("2.12.11", "2.13.3")

ThisBuild / scalaVersion := crossScalaVersions.value.head

scalacOptions ++= Seq(
  "-feature",
  "-deprecation"
)

libraryDependencies ++= Seq(
"com.typesafe.akka" %% "akka-http"   % "10.1.12",
"com.filez.scala.akka" %% "akka-http-msgpack" % "0.0.5",
"com.typesafe.akka" %% "akka-stream" % "2.6.6"
)

fork := true