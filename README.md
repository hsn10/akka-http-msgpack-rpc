MsgPack RPC support for Akka-HTTP
=================================

[MessagePack-RPC](https://github.com/msgpack-rpc/msgpack-rpc/blob/master/spec.md) is binary protocol for invoking
remote services using [MessagePack](https://github.com/msgpack/msgpack/blob/master/spec.md) as its serialization
format. It is commonly used using plain TCP socket or TLS socket as transport. Web browsers are using
[Websocket](https://en.wikipedia.org/wiki/WebSocket) transport.